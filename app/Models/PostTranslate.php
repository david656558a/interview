<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTranslate extends Model
{
    use HasFactory;

    protected $table = 'post_translate';
    protected $fillable = [
        'post_id',
        'lang',
        'name',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class,'post_id','id');
    }
}
