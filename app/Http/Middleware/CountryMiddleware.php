<?php

namespace App\Http\Middleware;

use App\Models\Country;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class CountryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return array
     */

    public static $mainLanguage = 'am';

    public static $languages = ['en','am'];

    public static function cacheQuery()
    {
        if(Schema::hasTable('countries')){
            return Country::all('lang')->pluck('lang')->toArray();
        }
    }

    public static function getLocale()
    {
        if (isset($_SERVER['REQUEST_URI'])){
            $countLengthString = $_SERVER['REQUEST_URI'];
        }else{
            $countLengthString = '/us/home';
        }

        $x = self::$languages = self::cacheQuery();

        $uri = substr($countLengthString, 1);

        $segmentsURI = explode('/',$uri);

        if ($x != null && !empty($segmentsURI[1]) && in_array($segmentsURI[1], self::$languages)) {
            return $segmentsURI[1];
        } else {
            return  self::$mainLanguage;
        }
    }



    public function handle(Request $request, Closure $next)
    {

        $locale = self::getLocale();

        $model = Country::query()->where('lang', $locale)->first();

        $lang = $model->lang;
        session()->put('lang', $lang);
        session()->put('defaultLang', self::$languages);
        session()->save();
        return $next($request);
    }
}
