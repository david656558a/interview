<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends LocalizedModel
{
    use HasFactory;

    protected $table = 'posts';
    protected $fillable = [

    ];

    public function lang (){
        return $this->hasMany(PostTranslate::class);
    }
}
