<?php


namespace App\Http\Helper;


class Helper
{


    public static function attribute($model)
    {
        $lang = session()->get('lang');
        $translate = $model->localization()->where('lang', $lang)->first();
        if ($translate){
            return $translate;
        }
        return null;
    }

}
