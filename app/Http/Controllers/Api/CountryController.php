<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Country\CreateRequest;
use App\Http\Requests\Country\EditRequest;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): \Illuminate\Http\Response
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        $country =  Country::create([
            'name' => $request->name,
            'lang' => $request->lang,
        ]);
        DB::commit();
        return response()->json([
            'status' => true,
            'message' => 'Successfully Created',
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EditRequest $request, $id): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        $country = Country::findOrFail($id);
        $country->update([
            'name' => $request->name,
            'lang' => $request->lang,
        ]);
        DB::commit();
        return response()->json([
            'status' => true,
            'message' => 'Successfully Updated',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        Country::findOrFail($id)->delete();

        return response()->json([
            'status' => true,
            'message' => 'Successfully Deleted',
        ], 200);
    }

    /**
     * @param $lang
     * @return \Illuminate\Http\JsonResponse
     */
     public function changeLanguage($lang): \Illuminate\Http\JsonResponse
     {
        $lang = Country::query()->where('lang', $lang)->first();
        if ($lang){
            session()->forget('lang');
            session()->flush();
            session()->put('lang', $lang->lang);
            session()->save();
            $message = 'Successfully changed';
        }else{
            $message = 'No this Country';
        }

        return response()->json([
            'status' => true,
            'message' => $message,
            'lang' => session()->get('lang'),
        ], 200);
    }




}
