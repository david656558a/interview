<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\PostTranslate;
use Illuminate\Database\Seeder;

class PostTranslateSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostTranslate::create([
           'post_id' => 1,
           'name' => 'Post Armenia 1',
           'lang' => 'am',
        ]);
        PostTranslate::create([
            'post_id' => 1,
            'name' => 'Post English 1',
            'lang' => 'en',
        ]);

        PostTranslate::create([
           'post_id' => 2,
           'name' => 'Post Armenia 2',
           'lang' => 'am',
        ]);
        PostTranslate::create([
            'post_id' => 2,
            'name' => 'Post English 2',
            'lang' => 'en',
        ]);
    }
}
