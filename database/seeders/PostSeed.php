<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Post;
use Illuminate\Database\Seeder;

class PostSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
           'id' => 1
        ]);
        Post::create([
            'id' => 2
        ]);
    }
}
