<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Country\CreateRequest;
use App\Http\Requests\Country\EditRequest;
use App\Http\Resources\Posts\GetPosts;
use App\Models\Country;
use App\Models\Post;
use App\Models\PostTranslate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id)
    {
        $post = Post::findOrFail($id)->load('translateLangInBlade');

        return response()->json([
            'status' => true,
            'data' => new GetPosts($post),
        ], 200);
    }

}
